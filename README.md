# Dark Google+ Theme
## 概要(Overview)
Google+のStylish,Stylus向けダークテーマです。
拡張機能のインストールが必要になります。現時点ではChrome,Firefox以外のブラウザ、Stylish,Stylus以外の拡張機能はサポート外となります。  

**拡張機能(Stylus)**:  
[![Chrome Web Store](res/Chrome.png "Chrome extension")](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)  [![Add-ons for Firefox](res/Firefox.png "Firefox extension")](https://addons.mozilla.org/ja/firefox/addon/styl-us/)
  
**テーマ本体**:  
[![Dark Google+ Theme](res/Theme.png "Theme")](https://userstyles.org/styles/153456/dark-google-plus-theme)
  

## 開発・貢献(Contribution)
開発・貢献は大歓迎です。Pull Requestはもちろん、issueもお待ちしております。  


## プレースホルダー(Place holder)
定義しているプレースホルダー

|ロゴ `logo`  |明色(Light)  |暗色(Dark)  |カラー(Colored)  |
|---|---|---|---|
|**Install key**  |light  |dark  |colored  |
|**Code**  |<details>`https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_light_color_88x24dp.png`</details>  |<details>`https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_dark_color_88x24dp.png`</details>  |<details>`https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_color_88x24dp.png`</details>  |
|**Preview**  |![light](https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_light_color_88x24dp.png)  |![dark](https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_dark_color_88x24dp.png)  |![colored](https://ssl.gstatic.com/images/branding/lockups/2x/lockup_gplus_color_88x24dp.png)  |

|トップバー `top_bar`  <br>  下部ナビゲーションバー `b_nav_bar`  |暗色(Dark)  |明るい赤(Light Red)  |暗い赤(Dark Red)  |ブルーグレー(Blue Gray)  |
|---|---|---|---|---|
|**Install key**  |dark  |light_red  |dark_red  |blue_gray  |
|**Code**  |`#424242`  |`#DB4437`  |`#D84315`  |`#37474F`  |

|背景色 `background`  |暗色(Dark)  |黑(Black)  |
|---|---|---|
|**Install key**  |dark  |black  |
|**Code**  |`#212121`  |`#000000`  |


## 派生(Derived Works)
 - **[Google+ for Franz](https://2001y.blogspot.com/2017/11/11-0.html?m=1)**  
FranzでGoogle+が利用できるようになるプラグインです。設定からダークテーマを有効化することによってこのテーマの派生版を利用できます。  


## ライセンス(License)
CC BY-NC-SA 4.0  

